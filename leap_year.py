""" Napisz funkcję leap_years, która przyjmuje parametr n i wyświetli n najbliższych lat przestępnych.
Zdefiniuj w tym celu funkcję is_leap, która zwróci wartość True gdy rok jest przestępny
(podzielny przez 4 i niepodzielny przez 100, lub podzielny przez 400:
https://pl.wikipedia.org/wiki/Rok_przest%C4%99pny), a następnie wykorzystaj pętlę while.
"""
import datetime


def leap_years(n):
    now = datetime.datetime.now()
    count = 0
    for d in range(now.year+1, now.year + n +1):
        if is_leap(d):
            count += 1
    return count


def leap_years1(n):
    now = datetime.datetime.now()
    count = 0
    i = 1
    while i != n +1:
        if is_leap(now.year + i):
            count += 1
        i+= 1
    return count


def is_leap(year):
    if (year % 4 == 0 and not year % 100 == 0) or year % 400 == 0:
        return True
    else:
        return False


if __name__ == "__main__":
    print(leap_years1(30))
    print(leap_years(30))
    print(leap_years(42))
    print(leap_year(90))